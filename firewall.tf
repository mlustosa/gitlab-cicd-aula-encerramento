// Define um recurso de regra de firewall no Google Compute Engine para permitir tráfego HTTP
resource "google_compute_firewall" "http-server" {
  name    = "permitir-http-padrao-terraform"
  network = "default"

  // Permite tráfego TCP de entrada na porta 80
  allow {
    protocol = "tcp"
    ports    = ["80"]
  }

  // Permite tráfego de todas as origens para instâncias com a tag "http-server"
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["http-server"]
}

resource "google_compute_firewall" "ssh" {
  name    = "permitir-ssh-padrao-terraform"
  network = "default"

  // Permite tráfego TCP de entrada na porta 80
  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  // Permite tráfego de todas as origens para instâncias com a tag "http-server"
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["ssh"]
}